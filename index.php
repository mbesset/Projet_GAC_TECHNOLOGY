<?php
session_start();
include("./include/fonction.php");
?>

<!DOCTYPE html>
<html>
<head>
    <?php include("./include/head.php");?>
</head>
<body>
    <div class="container-fluid">
        <div class="row top">
            <div class="col-3">
                BESSET Maxime
            </div>
            <div class="col-6">
                <h4>Projet GAC TECHNOLOGY</h4>
            </div>
            <div class="col-3">
                2018-03-03
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="col-12 import">
                    <?php
                    if (!empty($_SESSION['errors'])):
                        if($_SESSION['errors'] == 1){ ?>
                        <div class="alert alert-success">
                            <?php echo "Importation Réussie !"; ?>
                        </div>
                        <?php
                    }
                    else{ ?>
                    <div class="alert alert-danger">
                        <?php echo $_SESSION['errors']; ?>
                    </div>
                    <?php
                }
                    endif; ?>
                    <form action="./include/import.php" name="form_bdd" id="form_bdd" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="upload" value="ok">
                        <input type="file" name="fichiercsv" size="16">
                        <input type="submit" value="Upload">
                    </form>
                </div>
                <?php 
                request_sum_reel_time($bdd); 
                sum_sms_by_sub($bdd);
                ?>
            </div>
            <div class="col-6">
                <?php top_ten($bdd); ?>
            </div>
        </div>
    </div>
</body>
</html>
<?php unset($_SESSION['errors']); ?>
