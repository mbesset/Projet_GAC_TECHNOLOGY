<?php
include("./db/db.php");

function request_sum_reel_time($bdd){
    $requests="SELECT SEC_TO_TIME(sum(TIME_TO_SEC(`Durée/volume réel`))) AS SOMME "
    ."FROM tickets_appels "
    ."WHERE date <= '2012-02-15' "
    ."AND type LIKE '%appel%'";

    $values= $bdd->query($requests); ?>

    <div class="request_sum_reel_time">
        <p>Durée totale réelle des appels effectués après le 15/02/2012 (inclus) </p>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Durée total</th>
                </tr>
            </thead>
            <tbody>
                <?php while ( $value = $values->fetch()) { ?>
            <tr>
                <td><?php echo $value[0]?></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
<?php }

function top_ten($bdd){;
    $requests="SELECT `N° abonné`, sum(`Durée/volume facturé`) AS `Data consomée`"
    ."FROM tickets_appels "
    ."WHERE type LIKE '%connexion%' "
    ."AND `Heure` > '18:00:00' "
    ."OR `Heure` < '08:00:00' "
    ."GROUP BY `N° abonné` "
    ."ORDER BY `Data consomée` "
    ."DESC LIMIT 0,10";

    $values= $bdd->query($requests); ?>

    <div class="top_ten">
        <p>TOP 10 des volumes data facturés en dehors de la tranche horaire 8h00-18h00</p>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">N° abonné</th>
                    <th scope="col">Data consomée</th>
                </tr>
            </thead>
            <tbody>
                <?php while ( $value = $values->fetch()) { ?>
            <tr>
                <td><?php echo $value['N° abonné']?></td>
                <td><?php echo $value['Data consomée']?></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
<?php } 

function sum_sms_by_sub($bdd){
    $requests="SELECT COUNT(`id`) AS `Nb total SMS` "
    ."FROM `tickets_appels` "
    ."WHERE type LIKE '%envoi de sms%'";

    $values= $bdd->query($requests); ?>

    <div class="sum_sms_by_sub">
        <p>Quantité totale de SMS envoyés par l'ensemble des abonnés</p>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Nb total SMS envoyé</th>
                </tr>
            </thead>
            <tbody>
                <?php while ( $value = $values->fetch()) { ?>
            <tr>
                <td><?php echo $value['Nb total SMS']?></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
<?php } ?>
