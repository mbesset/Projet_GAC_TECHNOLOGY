<?php
session_start();
//Variable
$error = [];


if (isset($_POST['import'])) {
    $file = $_POST['file'];
    $content_dir = $_POST['content_dir'];
    $name_file = $_POST['name_file'];
 
    if (file_exists($file))
        $fp = fopen("$file", "r");
    else { /* le fichier n'existe pas */
        echo "Fichier introuvable !<br>Importation stoppée.";
        exit();
    }
    require '../db/db.php';
     while (!feof($fp)) //on importe le fichier
        { 
           $value = fgets($fp,4096);//tant qu'on est pas à la fin du fichier, on li une ligne

           $value = explode( ";",$value); // On récupère les champs séparés par ; dans liste

           //On assigne les variables
           $compte_fact = $value[0];
           $n_fact = $value[1]; 
           $n_abon = $value[2]; 
           $date = $value[3];
           $heure = $value[4];
           $dur_reel = $value[5];
           $dur_fact = $value[6];
           $type = $value[7];


           //On ajoute un nouvelle enregistrement dans la table
           $bdd->exec('INSERT INTO `tickets_appels` (`Compte facturé`,`N° Facture`,`N° abonné`,`Date`,`Heure`,`Durée/volume réel`,`Durée/volume facturé`,`type`) VALUES ("'.$compte_fact.'","'.$n_fact.'","'.$n_abon.'","'.$date.'","'.$heure.'","'.$dur_reel.'","'.$dur_fact.'","'.$type.'")');
        }

         //On ferme le fichier 
         fclose($fp); 
         $_SESSION['errors'] = 1;
         header('location: ../index.php');
 }
 
if (isset($_POST['upload'])) {
    if (empty($_FILES['fichiercsv']['tmp_name'])) {
            $_SESSION['errors'] = "Veuillez choisir un fichier CSV";
            header('location: ../index.php');
    } else {
        $content_dir = '../tmp/';
        $tmp_file = $_FILES['fichiercsv']['tmp_name'];
        if (!is_uploaded_file($tmp_file)) {
            exit("The file is lost");
        }
        $type_file = $_FILES['fichiercsv']['type'];
        $extensions_valides = array('csv', 'txt');
        $extension_upload = substr(strrchr($_FILES['fichiercsv']['name'], '.'), 1);
        if (in_array($extension_upload, $extensions_valides)) {
            // on copie le fichier dans le dossier de destination
            $name_file = $_FILES['fichiercsv']['name'];
            if (!move_uploaded_file($tmp_file, $content_dir . $name_file)) {
                exit("Impossible to copy the file to $content_dir");
            }
            $file = "$content_dir" . "$name_file";
            if (file_exists($file)) { ?>
                <form action="./import.php">
                    <input type="submit" value="Cancel">
                </form>
                <form action="" name="import" method="post">
                    <input type="hidden" name="file" value="<?php echo $file; ?>">
                    <input type="hidden" name="content_dir" value="<?php echo $content_dir; ?>">
                    <input type="hidden" name="name_file" value="<?php echo $name_file; ?>">
                    <input type="hidden" name="import" value="ok">
                    <input type="submit"  value ="Import into database">
                </form>
 
            <?php
                return;
            } else {
                exit("NO FILE EXIST");
            }
        } else {
            $_SESSION['errors'] = "Extention de fichier incorecte, Extention autoriser .csv !";
            header('location: ../index.php');
        }
    }
}
?>